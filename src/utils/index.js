export const searchCd = (...args) => {
    const keyword = args.shift()
    const result = []
    for (let i = 0; i < args.length; i++) {
      result.push(new RegExp(keyword, 'ig').test(args[i]))
    }
    return result.some(val => val)
  }