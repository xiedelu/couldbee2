import Vue from "vue";
import VueRouter from "vue-router";
import Home from "../views/Home.vue";
import Login from "../views/Login"
import Login2 from "../views/Login2"
import Login3 from "../views/Login3"
import Regist from "../views/Regist"
import Show from "../views/Show"
Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "Home",
    component: Home
  },
  {
    path: "/login",
    name: "Login",
    component: Login
  },
  {
    path: "/login2",
    name: "Login2",
    component: Login2
  },
  {
    path: "/login3",
    name: "Login3",
    component: Login3
  },
  {
    path: "/regist",
    name: "Regist",
    component: Regist
  },
  {
    path: "/show",
    name: "Show",
    component: Show
  },
  {
    path: "/about",
    name: "About",
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () =>
      import(/* webpackChunkName: "about" */ "../views/About.vue")
  }
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes
});

export default router;
