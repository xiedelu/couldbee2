import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import wheelDrawer from "@slevin/vue-wheel-drawer";
Vue.use(wheelDrawer);
import First from "./components/First";
import Vant from 'vant';
import 'vant/lib/index.css';
Vue.use(Vant);

import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
Vue.use(ElementUI);

import http from './utils/http'
Vue.prototype.$http = http


Vue.component("First", First);

Vue.config.productionTip = false;

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount("#app");
