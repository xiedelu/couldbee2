import Vue from 'vue'
import Vuex from 'vuex'
import { getItem, setItem } from '@/utils/storage.js'
const TOKEN = 'TOUTIAO_USER'

Vue.use(Vuex)
export default new Vuex.Store({
  state: {
    // 获取本地数据
    user: getItem(TOKEN)
  },
  mutations: {
    setUser (state, data) {
      state.user = data
      // 保存本地数据
      setItem(TOKEN)
    }
  }
})

